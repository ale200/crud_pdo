<?php
	if (isset($_GET['codCarrera'])){
		$id=intval($_GET['codCarrera']);
	} else {
		header("location:carrera.php");
    }
    
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CRUD con PHP usando Programación Orientada a Objetos</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/custom.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Editar <b>Carrera</b></h2></div>
                    <div class="col-sm-4">
                        <a href="carrera.php" class="btn btn-info add-new"><i class="fa fa-arrow-left"></i> Regresar</a>
                    </div>
                </div>
            </div>
            <?php
				
                include ("cCarrera.php");
               
				$carrera= new carrera();
				
				
				if(isset($_POST) && !empty($_POST)){
					$codCarrera = $carrera->sanitize($_POST['codCarrera']);
					$nombreCarrera = $carrera->sanitize($_POST['nombreCarrera']);
					$titulo = $carrera->sanitize($_POST['titulo']);
				   

					$id_carrera=intval($_POST['id_carrera']);
                    $res = $carrera->update($nombreCarrera, $titulo,
					$codCarrera,$id_carrera);
				

					if($res){
						$message= "Datos actualizados con éxito";
						$class="alert alert-success";
						
					}else{
						$message="No se pudieron actualizar los datos";
						$class="alert alert-danger";
					}
					
					?>
				<div class="<?php echo $class?>">
				  <?php echo $message;?>
				</div>	
					<?php
				}
				$datos_carrera=$carrera->single_record($id);
			?>
			<div class="row">
				<form method="post">
				<div class="col-md-6">
					<label>CodCarrera:</label>
					<input type="text" name="codCarrera" id="codCarrera" class='form-control' maxlength="100" required  value="<?php echo $datos_carrera->codCarrera;?>">
					<input type="hidden" name="id_carrera" id="id_carrera" class='form-control' maxlength="100"   value="<?php echo $datos_carrera->id;?>">
				</div>
				<div class="col-md-6">
					<label>NombreCarrera:</label>
					<input type="text" name="nombreCarrera" id="nombreCarrera" class='form-control' maxlength="100" required  value="<?php echo $datos_carrera->nombreCarrera;?>">
				</div>
				<div class="col-md-12">
					<label>Titulo:</label>
					<textarea  name="titulo" id="titulo" class='form-control' maxlength="255" required><?php echo $datos_carrera->titulo;?></textarea>
				</div>
				
				
				<div class="col-md-12 pull-right">
				<hr>
					<button type="submit" class="btn btn-success">Actualizar datos</button>
				</div>
				</form>
			</div>
        </div>
    </div>     
</body>
</html>