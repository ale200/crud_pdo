
<?php 
include ('Conexion.php');
$alumnos = new Database();
$listado=$alumnos->read()
?>



<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CRUD con PHP usando Programación Orientada a Objetos</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/custom.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Listado de  <b>Alumnos</b></h2></div>
                    <div class="col-sm-4">
                        <a href="create.php" class="btn btn-info add-new"><i class="fa fa-plus"></i> Agregar Alumnos</a>
                        <a href="index.php" class="btn btn-info add-new"><i class="fa fa-arrow-left"></i> Regresar</a>
                    </div>
                </div>
            </div>
            
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>CodAlumno</th>
                        <th>NombreCompleto</th>
                        <th>TipoDocumento</th>
                        <th>NumeroDocumento</th>
                        <th>Sexo</th>
                        <th>Correo</th>
                        <th>NumeroTelefono</th>
                        <th>CodCarrera</th>
                        <th>Direccion</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                 
                <tbody>   
                <?php 
while ($row=mysqli_fetch_object($listado)){
$codAlumno=$row->codAlumno;
$nombreCompleto=$row->nombreCompleto;
$tipoDocumento=$row->tipoDocumento;
$numeroDocumento=$row->numeroDocumento;
$sexo=$row->sexo;
$correo=$row->correo;
$numeroTelefono=$row->numeroTelefono;
$codCarrera=$row->codCarrera;
$direccion=$row->direccion;

?>
<tr>
<td><?php echo $codAlumno;?></td>
<td><?php echo $nombreCompleto;?></td>
<td><?php echo $tipoDocumento;?></td>
<td><?php echo $numeroDocumento;?></td>
<td><?php echo $sexo;?></td>
<td><?php echo $correo;?></td>
<td><?php echo $numeroTelefono;?></td>
<td><?php echo $codCarrera;?></td>
<td><?php echo $direccion;?></td>
<td>
<a href="update.php?codAlumno=<?php echo $codAlumno;?>" class="edit" title="Editar" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
<a href="delete.php?codAlumno=<?php echo $codAlumno;?>" class="delete" title="Eliminar" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
</td>
</tr>	
<?php
}
?> 
                          
                </tbody>
            </table>
        </div>
    </div>   

    
